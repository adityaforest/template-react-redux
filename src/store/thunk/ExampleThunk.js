import { createAsyncThunk } from "@reduxjs/toolkit";

export const exampleThunk = createAsyncThunk(
    'exampleThunk',
    async (input) => {
        //thunk acts like a middleware for redux
        //you can put async function or standard function here
        try{
            console.log('redux thunk get this input : ' + input)     
            return input       
        }catch(err){
            console.log('redux thunk get this error : ' + err)   
            return err         
        }
    }
)