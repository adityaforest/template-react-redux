import { configureStore } from "@reduxjs/toolkit";
import ExampleReducer from "./reducers/ExampleReducer";

export const store = configureStore({
    reducer:{
        ExampleReducer,
    }
})