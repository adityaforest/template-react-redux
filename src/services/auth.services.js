import axios from 'axios'

//don't forget to create .env.local files with NEXT_PUBLIC_ variables
const API_URL = `${process.env.NEXT_PUBLIC_BE_URL}/api/v1/auth`

class AuthServices {
    login(email, password) {
        return axios
            .post(API_URL + "login", { email, password })
            .then((response) => {
                // console.log(response.data)
                if (response.data.data.token) {
                    localStorage.setItem("user", JSON.stringify(response.data.data));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user')
    }

    register(email, password, firstName, lastName) {
        return axios.post(API_URL + "register", {
            email,
            password,
            firstName,
            lastName,
        });
    }

    isLoggedIn() {        
        const user = JSON.parse(localStorage.getItem("user"));
                
        if (user) {
            return user
        }        
        else {
            return false
        }
    }
}

export default new AuthServices