import axios from 'axios';
import authHeader from './auth.header';

const API_URL = `${process.env.NEXT_PUBLIC_BE_URL}/api/v1/otheraAPI/`;

class OtherServices {  

  getAll() {
    return axios.get(API_URL , { headers: authHeader() });
  }

  getById(id) {
    return axios.get(API_URL + id, { headers: authHeader() }); //put id here
  }

}

export default new OtherServices();